package com.ansarul.ExperisFullstackDeveloper.controllers;

import com.ansarul.ExperisFullstackDeveloper.models.Managers;
import com.ansarul.ExperisFullstackDeveloper.repositories.ManagersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/managers")
public class ManagersControllers {
    @Autowired
    private ManagersRepository managersRepository;

    @GetMapping()
    public ResponseEntity<List<Managers>> getAllManagers() {
        List<Managers> managers = managersRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(managers, resp);
    }

    @PostMapping
    public ResponseEntity<Managers> addManagers(@RequestBody Managers managers) {
        Managers returnManagers = managersRepository.save(managers);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnManagers, status);
    }
}
