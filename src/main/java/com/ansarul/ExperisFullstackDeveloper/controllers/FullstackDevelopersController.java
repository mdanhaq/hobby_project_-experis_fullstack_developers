package com.ansarul.ExperisFullstackDeveloper.controllers;

import com.ansarul.ExperisFullstackDeveloper.models.FullstackDevelopers;
import com.ansarul.ExperisFullstackDeveloper.repositories.FullstackDeveloperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/developers")
public class FullstackDevelopersController{

    @Autowired
    private FullstackDeveloperRepository fullstackDeveloperRepository;

    @GetMapping()
    public ResponseEntity<List<FullstackDevelopers>> getAllFullstackDevelopers() {
        List<FullstackDevelopers> fullstackDevelopers = fullstackDeveloperRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(fullstackDevelopers, resp);
    }

    @PostMapping
    public ResponseEntity<FullstackDevelopers> addFullstackDeveloper(@RequestBody FullstackDevelopers fullstackDevelopers) {
        FullstackDevelopers returnFullstackDevelopers = fullstackDeveloperRepository.save(fullstackDevelopers);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFullstackDevelopers, status);
    }
}
