package com.ansarul.ExperisFullstackDeveloper.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Managers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "FullName")
    private String fullName;

    @Column(name = "Position")
    private String position;

    @Column(name = "Email")
    private String email;

    @Column(name = "Tlf")
    private String tlf;

    @ManyToMany
    @JoinTable(
            name = "fullstackdevelopers_managers",
            joinColumns = {@JoinColumn(name = "managers_id")},
            inverseJoinColumns = {@JoinColumn(name = "fullstackdevelopers_id")}
    )
    public List<FullstackDevelopers> fullstackDevelopers = new ArrayList<>();

    @JsonGetter("fullstackDevelopers")
    public List <String> fullstackdevelopersGetter() {
        if (fullstackDevelopers!= null) {
            return fullstackDevelopers.stream()
                    .map(fullstackDeveloper -> {
                        return "/api/v1/developers/" + fullstackDeveloper.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTlf() {
        return tlf;
    }

    public void setTlf(String tlf) {
        this.tlf = tlf;
    }
}
