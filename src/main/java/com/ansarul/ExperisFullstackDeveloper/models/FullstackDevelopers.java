package com.ansarul.ExperisFullstackDeveloper.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class FullstackDevelopers {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "FullName")
    private String fullName;
    @Column(name = "Title")
    private String title;
    @Column(name = "WorksAt")
    private String worksAt;
    @Column(name = "Country")
    private String countryOfOrigin;
    @Column(name = "Telephone")
    private String tlf;

    @ManyToMany
    @JoinTable(
            name = "fullstackdevelopers_managers",
            joinColumns = {@JoinColumn(name = "fullstackdevelopers_id")},
            inverseJoinColumns = {@JoinColumn(name = "managers_id")}
    )
    public List<Managers> managers = new ArrayList<>();

    @JsonGetter("managers")
    public List <String> managersGetter() {
        if (managers!= null) {
            return managers.stream()
                    .map(manager -> {
                        return "/api/v1/managers/" + manager.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getWorksAt() {
        return worksAt;
    }

    public void setWorksAt(String worksAt) {
        this.worksAt = worksAt;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public String getTlf() {
        return tlf;
    }

    public void setTlf(String tlf) {
        this.tlf = tlf;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
