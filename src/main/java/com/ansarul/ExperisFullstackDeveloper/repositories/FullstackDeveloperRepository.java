package com.ansarul.ExperisFullstackDeveloper.repositories;

import com.ansarul.ExperisFullstackDeveloper.models.FullstackDevelopers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface FullstackDeveloperRepository extends JpaRepository<FullstackDevelopers, Long> {

}
