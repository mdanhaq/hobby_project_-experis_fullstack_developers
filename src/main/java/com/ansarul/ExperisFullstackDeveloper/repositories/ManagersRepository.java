package com.ansarul.ExperisFullstackDeveloper.repositories;

import com.ansarul.ExperisFullstackDeveloper.models.Managers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagersRepository extends JpaRepository<Managers, Long> {
}
