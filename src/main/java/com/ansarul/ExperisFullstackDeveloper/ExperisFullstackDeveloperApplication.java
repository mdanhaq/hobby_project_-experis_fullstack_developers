package com.ansarul.ExperisFullstackDeveloper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExperisFullstackDeveloperApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExperisFullstackDeveloperApplication.class, args);
	}

}
