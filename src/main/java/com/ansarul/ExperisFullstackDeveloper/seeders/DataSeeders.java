package com.ansarul.ExperisFullstackDeveloper.seeders;

import com.ansarul.ExperisFullstackDeveloper.models.FullstackDevelopers;
import com.ansarul.ExperisFullstackDeveloper.models.Managers;
import com.ansarul.ExperisFullstackDeveloper.repositories.FullstackDeveloperRepository;
import com.ansarul.ExperisFullstackDeveloper.repositories.ManagersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataSeeders {
    private DataSeeders dataSeeders;

    @Autowired
    private FullstackDeveloperRepository fullstackDeveloperRepository;
    @Autowired
    private ManagersRepository managersRepository;

    private boolean emptyDatabase = false;

    @EventListener
    public void seedData(ContextRefreshedEvent event) {
        if (fullstackDeveloperRepository.findAll().isEmpty() && !emptyDatabase)
            seedFullstackDevelopers();

        if(managersRepository.findAll().isEmpty() && !emptyDatabase)
            seedManagers();
    }

    private void seedFullstackDevelopers() {
        FullstackDevelopers fullstackDevelopersOne = new FullstackDevelopers();
        fullstackDevelopersOne.setFullName("Ansarul Haque");
        fullstackDevelopersOne.setCountryOfOrigin("Bangladesh");
        fullstackDevelopersOne.setTitle("Fullstack Developer");
        fullstackDevelopersOne.setTlf("+4531892677");
        fullstackDevelopersOne.setWorksAt("Experis");
        fullstackDeveloperRepository.save(fullstackDevelopersOne);

        FullstackDevelopers fullstackDevelopersTwo = new FullstackDevelopers();
        fullstackDevelopersTwo.setFullName("Mosiur Rahman");
        fullstackDevelopersTwo.setCountryOfOrigin("Bangladesh");
        fullstackDevelopersTwo.setTitle("Fullstack Developer");
        fullstackDevelopersTwo.setTlf("+4542241588");
        fullstackDevelopersTwo.setWorksAt("Experis");
        fullstackDeveloperRepository.save(fullstackDevelopersTwo);

        FullstackDevelopers fullstackDevelopersThree = new FullstackDevelopers();
        fullstackDevelopersThree.setFullName("Mikkel Shumakhar");
        fullstackDevelopersThree.setCountryOfOrigin("Denmark");
        fullstackDevelopersThree.setTitle("Fullstack Developer");
        fullstackDevelopersThree.setTlf("+45 ......");
        fullstackDevelopersThree.setWorksAt("Smartpage");
        fullstackDeveloperRepository.save(fullstackDevelopersThree);

        FullstackDevelopers fullstackDevelopersFour = new FullstackDevelopers();
        fullstackDevelopersFour.setFullName("Atmar Khan");
        fullstackDevelopersFour.setCountryOfOrigin("Afganistan");
        fullstackDevelopersFour.setTitle("Fullstack Developer");
        fullstackDevelopersFour.setTlf("+45 ......");
        fullstackDevelopersFour.setWorksAt("DXC");
        fullstackDeveloperRepository.save(fullstackDevelopersFour);

        FullstackDevelopers fullstackDevelopersFive = new FullstackDevelopers();
        fullstackDevelopersFive.setFullName("Simon Reddy");
        fullstackDevelopersFive.setCountryOfOrigin("India");
        fullstackDevelopersFive.setTitle("Fullstack Developer");
        fullstackDevelopersFive.setTlf("+45 ......");
        fullstackDevelopersFive.setWorksAt("Experis");
        fullstackDeveloperRepository.save(fullstackDevelopersFive);
    }

    private void seedManagers() {
        Managers managersOne = new Managers();
        managersOne.setFullName("Daniel ");
        managersOne.setPosition("Head of Experis");
        managersOne.setEmail("!!!!!!");
        managersOne.setTlf("+45 !!!!!!");
        managersRepository.save(managersOne);

        Managers managersTwo = new Managers();
        managersTwo.setFullName("Jesper Fenn");
        managersTwo.setPosition("Manager of Experis");
        managersTwo.setEmail("!!!!!!");
        managersTwo.setTlf("+45 !!!!!!");
        managersRepository.save(managersTwo);
    }
}
