## Project Name
Experis Fullstack Developers and Experis Managers Management

## Description
This purpose of this project is simply maintaining Experis Fullstack developers and managers information which could help the future newcomers to get in touch with their alumni contact information and make networks.

For this purpose, I have made two models (FullstackDevelopers and Managers), used repository pattern, restcontrollers for Web APIs and seeded data beforhand to give project default primary data. 

## Installation
To run this project, someone needs Spring Boot with Spring Web, Spring Data JPA and PostgreSQL Driver as dependency. I have used Spring Initializr from https://start.spring.io/ to make jar file and then proceeded with that one. For maintaining the database PostgreSQL was my choice.  

## Contributing
I am open to contributions and welcome any constructive critics about this project.

## Authors and acknowledgment
I would love to thank my colleague Mosiur Rahman to be a part of this project and contribution with his insights.

## Project status
This project is not up to my desiring destination. But it will get its shape in the near future. 

